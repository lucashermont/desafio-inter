# Desafio Banco Inter

Esse projeto é uma API para criação de usuários com digitos únicos

## Começando

Para executar o projeto, será necessário instalar os seguintes programas:

- [JDK: Necessário para executar o projeto Java]
- [Maven: Necessário para realizar o build do projeto Java]
- [Eclipse ou STS: Para desenvolvimento do projeto]


## Desenvolvimento

Para iniciar o desenvolvimento, é necessário clonar o projeto do GitHub num diretório de sua preferência:

```shell
cd "diretorio de sua preferencia"
git clone https://github.com/lucashmm94/banco-inter.git
```

### Construção

Para construir o projeto com o Maven, executar os comando abaixo:

```shell
mvn clean install
```

O comando irá baixar todas as dependências do projeto e criar um diretório *target* com os artefatos construídos, que incluem o arquivo jar do projeto. 
Além disso, serão executados os testes unitários, e se algum falhar, o Maven exibirá essa informação no console.


## Testes

Para rodar os testes, utilize o comando abaixo:

```
mvn test
```


## Links
Necessário rodar o sistema:
	Banco h2 parametros: 
		*Driver class: org.h2.Driver
		*JDBC url: jdbc:h2:mem:testdb
		*User Name: sa
- [Banco em memória](http://localhost:8080/h2-console/)
- [API documentada swagger](http://localhost:8080/h2-console/)


