package br.com.lucasti.desafio.entities;

import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class DigitoUnicoTest {

	@Test
	public void calcularDigitoUnicoTest() {
		DigitoUnico digitoUnico = new DigitoUnico("396",4, null);
		assertTrue(9 == digitoUnico.getDigitoUnico());
	}
	
}
