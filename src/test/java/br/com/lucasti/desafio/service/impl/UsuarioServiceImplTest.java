package br.com.lucasti.desafio.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import br.com.lucasti.desafio.entities.DigitoUnico;
import br.com.lucasti.desafio.entities.Usuario;
import br.com.lucasti.desafio.repositories.DigitoUnicoRepository;
import br.com.lucasti.desafio.repositories.UsuarioRepository;
import br.com.lucasti.desafio.services.impl.UsuarioServiceImpl;
import br.com.lucasti.desafio.services.impl.exception.EntityNotFoundExceptionById;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest
public class UsuarioServiceImplTest {
	
	public static final String CHAVE_PUBLICA = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiE71Tj7dVL7Ihiemcc2gsBCiGp+Cc2qg75s7iioLQfri5+7KMuFRf0WDl7xPhEyi5RiUmNU8esdx78SMsEA8ImXUFdpUMAQVKDH41voMsT55XcBO7xJwPz5fofjFldMU2ppiE7CeasWA7vW5noez4/RJcR9DjAPpgcchqWR/WY7Zpjm++Ni3+fEa52P8TIyuI750TlmsvNyyqCVuwxlI7ZtintZfR4NTKolLV9EymAE1g5bacysn8r5NaVR8/gggZ/+7vWamhwR3dAauWpBwkQ1JM9+xXMnLV6IXOpgZ2WERUTx+nAjN8ycH4HFOOuEp6p+hrCQTfea0wAnD9gd9ZwIDAQAB";

	@Autowired
	UsuarioServiceImpl service;

	@Autowired
	UsuarioRepository repository;
	
	@Autowired
	DigitoUnicoRepository digitoUnicoRepository;

	@Test
	public void saveTest() {
		Usuario usuario = new Usuario("Lucas", "lucas.hermont@hotmail.com");

		service.save(usuario);

		assertThat(usuario.getId()).isNotNull();
		assertThat(usuario.getNome()).isEqualTo("Lucas");
		assertThat(usuario.getEmail()).isEqualTo("lucas.hermont@hotmail.com");

	}

	@Test
	public void saveTestFailWithoutEmail() {
		Assertions.assertThrows(DataIntegrityViolationException.class, () -> {
			Usuario usuario = new Usuario();
			usuario.setNome("Lucas");
			service.save(usuario);
		});
	}

	@Test
	public void saveTestFailWithoutName() {
		Assertions.assertThrows(DataIntegrityViolationException.class, () -> {
			Usuario usuario = new Usuario();
			usuario.setEmail("lucas.hermont@hotmail.com");
			service.save(usuario);
		});
	}

	@Test
	public void findByIdTest() {
		Usuario usuario = new Usuario("Lucas", "lucas.hermont@hotmail.com");

		service.save(usuario);

		assertTrue(repository.existsById(usuario.getId()));
	}

	@Test
	public void findByIdNotExisted() {
		Assertions.assertThrows(EntityNotFoundExceptionById.class, () -> {
			service.findById(17L);
		});
	}

	@Test
	public void findALL() {
		this.repository.deleteAll();

		Usuario usuario = new Usuario("Lucas", "lucas.hermont@hotmail.com");
		Usuario usuario2 = new Usuario("Lucas", "lucas.hermont@hotmail.com");

		service.save(usuario);
		service.save(usuario2);

		assertSame(service.findAll().size(), 2);
	}

	@Test
	public void updateTest() {
		Usuario usuario = new Usuario("lucas", "lucas.hermont@hotmail.com");
		service.save(usuario);

		usuario.setEmail("teste@hotmail.com");
		usuario.setNome("teste");

		service.update(usuario, usuario.getId());

		assertTrue("teste".equalsIgnoreCase(service.findById(usuario.getId()).getNome()));
		assertTrue("teste@hotmail.com".equalsIgnoreCase(service.findById(usuario.getId()).getEmail()));
	}

	@Test
	public void updateTestFailUserWithIdInvalid() {
		Assertions.assertThrows(EntityNotFoundExceptionById.class, () -> {
			Usuario usuario = new Usuario("lucas", "lucas.hermont@hotmail.com");
			service.save(usuario);

			usuario.setEmail("teste@hotmail.com");
			usuario.setNome("teste");

			service.update(usuario, 20L);
		});

	}

	@Test
	public void updateTestFailUserWithoutName() {
		Assertions.assertThrows(DataIntegrityViolationException.class, () -> {
			Usuario usuario = new Usuario("lucas", "lucas.hermont@hotmail.com");
			service.save(usuario);

			usuario.setEmail("teste@hotmail.com");
			usuario.setNome(null);

			service.update(usuario, usuario.getId());
		});

	}
	
	@Test
	public void updateTestFailUserWithoutEmail() {
		Assertions.assertThrows(DataIntegrityViolationException.class, () -> {
			Usuario usuario = new Usuario("lucas", "lucas.hermont@hotmail.com");
			service.save(usuario);

			usuario.setEmail(null);
			usuario.setNome("Lucas");

			service.update(usuario, usuario.getId());
		});

	}
	
	@Test
	public void deleteByIdTest() {
		Usuario usuario = new Usuario("lucas", "lucas.hermont@hotmail.com");
		service.save(usuario);

		service.deleteById(usuario.getId());

		assertThat(repository.existsById(usuario.getId())).isFalse();
	}
	
	@Test
	public void deleteByIdTestWithIdInvalid() {
		Assertions.assertThrows(EntityNotFoundExceptionById.class, () -> {
			service.findById(55L);
		});
	}
	
	@Test
	public void listaDigitoPorUsuarioTest () {
		Usuario usuario = new Usuario("Lucas","lucas.hermont@hotmail.com");
		service.save(usuario);
		
		DigitoUnico dG = new DigitoUnico("2", 5, usuario);
		DigitoUnico dG2 = new DigitoUnico("2", 5, usuario);
		
		digitoUnicoRepository.save(dG);
		digitoUnicoRepository.save(dG2);
		
		List<DigitoUnico> lista = service.listaDigitoPorUsuario(usuario.getId());
		
		assertThat(lista.size() ==2);
		assertThat(lista.contains(dG));
		
	}
	
	@Test
	public void criptografarInformacoesUsuarioTest() throws IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException {
		Usuario usuario = new Usuario("Lucas","lucas.hermont@hotmail.com");
		repository.save(usuario);
		
		service.criptografarInformacoesUsuario(1L, CHAVE_PUBLICA);
		
		assertThat(service.findById(usuario.getId())).isNotNull();
		assertThat(service.findById(usuario.getId()).getNome().equals("Lucas"));
		assertThat(service.findById(usuario.getId()).getEmail().equals("lucas.hermont@hotmail.com"));
		
	}


}
