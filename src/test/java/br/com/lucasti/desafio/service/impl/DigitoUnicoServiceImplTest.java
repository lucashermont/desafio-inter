package br.com.lucasti.desafio.service.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import br.com.lucasti.desafio.entities.DigitoUnico;
import br.com.lucasti.desafio.entities.Usuario;
import br.com.lucasti.desafio.services.impl.DigitoUnicoServiceImpl;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest
public class DigitoUnicoServiceImplTest {

	@Autowired
	DigitoUnicoServiceImpl service;
	
	@Test
	public void calcularDigitoUnicoTest() {
		service.saveDigitoUnico(new DigitoUnico("2", 11, null));
	}
	
	@Test
	public void calcularDigitoUnicoTestNGreateAllowed() {
		Assertions.assertThrows(NumberFormatException.class,() ->{
			service.saveDigitoUnico(new DigitoUnico("100000001", 11, null));
		});
		
	}
	
	@Test
	public void calcularDigitoUnicoTestKGreateAllowed() {
		Assertions.assertThrows(NumberFormatException.class,() ->{
			service.saveDigitoUnico(new DigitoUnico("2", 1000001, null));
		});
		
	}
	
	@Test
	public void calcularDigitoUnicoTestKUserNotPersistido() {
		Assertions.assertThrows(InvalidDataAccessApiUsageException.class,() ->{
			service.saveDigitoUnico(new DigitoUnico("2", 10, new Usuario("lucas", "lucas@hotmail.com")));
		});
		
		
		
	}
	
	
	
}
