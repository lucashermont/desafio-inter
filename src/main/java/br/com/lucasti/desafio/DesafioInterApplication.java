package br.com.lucasti.desafio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioInterApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioInterApplication.class, args);
	}

}
