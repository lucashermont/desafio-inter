package br.com.lucasti.desafio.security;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.tomcat.util.codec.binary.Base64;

public class Criptografia {

	public static final String CHAVE_PRIVADA = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCITvVOPt1UvsiGJ6ZxzaCwEKIan4JzaqDvmzuKKgtB+uLn7soy4VF/RYOXvE+ETKLlGJSY1Tx6x3HvxIywQDwiZdQV2lQwBBUoMfjW+gyxPnldwE7vEnA/Pl+h+MWV0xTammITsJ5qxYDu9bmeh7Pj9ElxH0OMA+mBxyGpZH9ZjtmmOb742Lf58RrnY/xMjK4jvnROWay83LKoJW7DGUjtm2Ke1l9Hg1MqiUtX0TKYATWDltpzKyfyvk1pVHz+CCBn/7u9ZqaHBHd0Bq5akHCRDUkz37FcyctXohc6mBnZYRFRPH6cCM3zJwfgcU464Snqn6GsJBN95rTACcP2B31nAgMBAAECggEANzS7d3BQlhUbA/KlH+xnS0fBT8UQwYwLGGAnsiKrIpLpOYRmEktdhY1y6Y6UKgIb62uUK34Zm6SRiclyLaQmdswFJMCjXjsZEomCvhbR8ytZXlYX4g32tjad5uzpoW+3P35NgKoEzzukp6OQBRvzCL0UzaMa6WdgOHadNI0GYw+30nkvKI7HahS4L8j4MdpMu04/Z9Uv5Xauj0LXO71ZST5bDTyGkE2dCBhuYpityYXf4k5DL3QDbrhsoiQ5NgcFKTYPY12jinmglVPQl+LZC9KRYSkwb4+y/SuzPnacqyzByILRX8JVH0JK6VFB/Ko6Wr5KkYN4Qy2fMmfJUfRvMQKBgQC+FgPwZMXuuQmSYrMxUzKkf1vW1IMKTLKnrp2QX2/IHAOVUz5DtJqocC34psGBAFBzE06ml/Vp6h9dbbqoeuRfdcza+MhdPqK8h/0ovJCUCBSpiBg3tkiq2Rd8vVROzrZlaRZiV1dnV+qv2tdLjgrmRCtnlIt5sBboAFy8B0BDfwKBgQC3kxgJh4uk/Z8bwm9ES/krhyIJbbgLR5pCURTrBydnvJmQZwSpOkWoe8lzluxo3BWhOuWl434qaf7SaBiHjfftHeCpGq2OBQV31+ehkgueeXJn3oY2lVhaddrBveVBFeYQa+26fAmJFKFBRhMxiiSPWhmhiKQ+gC6zRtjPoRoaGQKBgE2FHvAfuNTlY3DujZOVEXcidWrEWfiOVOGrhZiGQ0kBm7h/LUphUh1VOnpIT2vzm9uCSTYJjM037LimclQZYh9IJFY81UGptKZcczL0y6WDMiH9XZsC3NnIFRsK/2HwTVRIKhfK9NsxcIVtIxYuBGXp8SWZ39Otq3nMBAWuk1ezAoGAHwk3XyUTbumSJNdSiHFvxvQpxP5Px1hrtBqRNssW1msEa21/VD0N2dZT0L8LaQ8MeR3EBa/YfMM+2cPPodtWtzf965kfDou6yPAvsRlH7ZsKxJTSjBfjwCd8vJSmVcKmCtC0eHrQAeUC/k4ioJWCaNoKy0yWuBpjytuHfFL4XsECgYBkhoBqv358unE+fFLbNw0+Pacvsa0TC7C82TylkpdzmkEgJMuZdckKr6FQNHeERGnIQHmBTpMS2Z71a3WJFk7zPU9bP0JQmTgkFbZqODa7PoHpr/I2c+oBOqU4vTgjy8I0S8ULUx12i1daksg4Lx2Hsyftjgq5lR/I0AoYNAZ+EA==";
	public static final String ALGORITHM = "RSA";

	private PublicKey publicKey;
	private PrivateKey privateKey;
	private Cipher cipher;

	public Criptografia(PublicKey publicKey, PrivateKey privateKey)
			 {
		this.setPublicKey(publicKey);
		this.privateKey = privateKey;
		try {
			this.cipher = Cipher.getInstance(ALGORITHM);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw new RuntimeException("Algoritmo não disponível no ambiente! Exemplo de algoritmo: RSA");
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
			throw new RuntimeException("Chaves inválidas para criação do objeto de criptografia: cipher");
		}
	}

	public PublicKey getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(PublicKey publicKey) {
		this.publicKey = publicKey;
	}

	public PrivateKey getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(PrivateKey privateKey) {
		this.privateKey = privateKey;
	}

	public Cipher getCipher() {
		return cipher;
	}

	public void setCipher(Cipher cipher) {
		this.cipher = cipher;
	}

	public static KeyPair genKeyPair() {
		KeyPairGenerator generator;
		try {
			generator = KeyPairGenerator.getInstance(ALGORITHM);
			generator.initialize(2048);
			KeyPair keyPair = generator.generateKeyPair();
			return keyPair;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw new RuntimeException("Algoritmo não disponível no ambiente! Exemplo de algoritmo: RSA");
		}

	}

	public static PublicKey genPublicKey(String publicKeyContent) {

		try {
			byte[] buffer = Base64.decodeBase64(publicKeyContent);
			KeyFactory kf;
			kf = KeyFactory.getInstance(ALGORITHM);
			X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(buffer);
			RSAPublicKey pubKey = (RSAPublicKey) kf.generatePublic(keySpecX509);

			return pubKey;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw new RuntimeException("Algoritmo não disponível no ambiente! Exemplo de algoritmo: RSA");
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
			throw new RuntimeException("Erro ao fazer decode da string da chave recebida");
		}

	}

	public static PrivateKey genPrivateKey(String privateKeyContent) {

		try {
			byte[] buffer = Base64.decodeBase64(privateKeyContent);
			KeyFactory kf;
			kf = KeyFactory.getInstance(ALGORITHM);
			PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(buffer);
			PrivateKey privKey = kf.generatePrivate(keySpecPKCS8);

			return privKey;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw new RuntimeException("Algoritmo não disponível no ambiente! Exemplo de algoritmo: RSA");
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
			throw new RuntimeException("Erro ao fazer decode da string da chave recebida");
		}

	}

	public String criptografar(String msg) {
		try {
			this.cipher.init(Cipher.ENCRYPT_MODE, publicKey);
			return Base64.encodeBase64String(cipher.doFinal(msg.getBytes("UTF-8")));
		} catch (InvalidKeyException e) {
			e.printStackTrace();
			throw new RuntimeException("String passada para conversão de chave inválida!");
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
			throw new RuntimeException("Não foi possível criptpgrafar dados!");
		} catch (BadPaddingException e) {
			e.printStackTrace();
			throw new RuntimeException("Parametro passado erro ao criptografar/descriptografar");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new RuntimeException("Não foi possível criptografar: Codificação de caracteres não disponível");
		}
	}

	public String descriptografar(String msg){
		try {
			this.cipher.init(Cipher.DECRYPT_MODE, privateKey);
			return new String(cipher.doFinal(Base64.decodeBase64(msg)), "UTF-8");
		} catch (InvalidKeyException e) {
			e.printStackTrace();
			throw new RuntimeException("String passada para conversão de chave inválida!");
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
			throw new RuntimeException("Não foi possível criptpgrafar dados!");
		} catch (BadPaddingException e) {
			e.printStackTrace();
			throw new RuntimeException("Parametro passado erro ao criptografar/descriptografar");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new RuntimeException("Não foi possível criptografar: Codificação de caracteres não disponível");
		}
	}

}
