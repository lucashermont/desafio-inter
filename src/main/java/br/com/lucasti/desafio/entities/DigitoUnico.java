package br.com.lucasti.desafio.entities;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name = "tb_digito_unico")
public class DigitoUnico implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "valor_digito_unico")
	private Integer digitoUnico;

	@Column(name = "valor_n", nullable = false, length = 10000000)
	private String n;

	@Column(name = "valor_k", nullable = false, length = 100000)
	private Integer k;

	@JsonProperty(access = Access.WRITE_ONLY)
	@ManyToOne
	@JoinColumn(name = "usuario_id")
	private Usuario usuario;

	@Transient
	static Map<String, Integer> cache = new LinkedHashMap<String, Integer>();

	public DigitoUnico() {
	}

	public DigitoUnico(String n, Integer k, Usuario usuario) {
		this.n = n;
		this.k = k;
		this.usuario = usuario;
		verificarCache();
	}

	private void verificarCache() {
		String numeroConcatenado = this.concatenarNumeros(this.n, this.k);
		if (cache.containsKey(numeroConcatenado))
			this.digitoUnico = cache.get(numeroConcatenado);
		else {
			this.digitoUnico = this.getCalcularDigitoUnico(numeroConcatenado);
			if (cache.size() < 10) {
				cache.put(numeroConcatenado, digitoUnico);
			} else {
				cache.remove(cache.keySet().toArray()[0]);
				cache.put(numeroConcatenado, digitoUnico);
			}
		}
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public String getN() {
		return n;
	}

	public void setN(String n) {
		this.n = n;
	}

	public Integer getK() {
		return k;
	}

	public void setK(Integer k) {
		this.k = k;
	}

	public Integer getDigitoUnico() {
		return digitoUnico;
	}

	public void setDigitoUnico(Integer digitoUnico) {
		this.digitoUnico = digitoUnico;
	}

	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Integer getCalcularDigitoUnico(String n) {
		Integer digitoUnico = 0;
		Long x = Long.parseLong(n);
		Long soma = 0L;

		if (x < 10)
			return x.intValue();
		else {
			while (x > 0) {
				soma += (x % 10);
				x /= 10;
			}
			digitoUnico = soma.intValue();
		}

		if (digitoUnico > 9) {
			return getCalcularDigitoUnico(digitoUnico.toString());
		}

		return digitoUnico;
	}

	private final String concatenarNumeros(String n, Integer k) {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < k; i++) {
			sb.append(n);
		}
		return sb.toString();

	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Parametro n: ").append(this.n).append("\nParametro k: ").append(this.k).append("\nDigito único: ")
				.append(this.digitoUnico);
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DigitoUnico other = (DigitoUnico) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
