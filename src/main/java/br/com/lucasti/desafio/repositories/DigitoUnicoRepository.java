package br.com.lucasti.desafio.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.lucasti.desafio.entities.DigitoUnico;

public interface DigitoUnicoRepository extends JpaRepository<DigitoUnico, Long>{

}
