package br.com.lucasti.desafio.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.lucasti.desafio.entities.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long>{

}
