package br.com.lucasti.desafio.config;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import br.com.lucasti.desafio.services.DigitoUnicoService;
import br.com.lucasti.desafio.services.UsuarioService;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
@Profile("dev")
public class DevConfig implements CommandLineRunner {
	
	@Autowired DigitoUnicoService digitoUnicoService;
	@Autowired UsuarioService usuarioService; 
	
	
	@Bean
    public Docket apis() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("br.com.lucasti.desafio"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }
	
	private ApiInfo apiInfo() {
		
        return new ApiInfo(
                "API para o desafio do banco inter",
                "API REST para cadastro de usuario e digito único",
                "0.01",
                "",
                new Contact("Lucas Hermont","https://www.linkedin.com/in/lucas-hermont-0b3a54180/", "lucas.hermont@hotmail.com"),
                "",
                "",
                new ArrayList<>()
        );
		 
    }
	
	@Override
	public void run(String... args) throws Exception {
		
	}
	
}




    

