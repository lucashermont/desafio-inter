package br.com.lucasti.desafio.controllers;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.lucasti.desafio.entities.DigitoUnico;
import br.com.lucasti.desafio.entities.Usuario;
import br.com.lucasti.desafio.services.UsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/usuario")
@Api(value="API REST Usuario")
public class UsuarioController {

	@Autowired
	UsuarioService service;

	@ApiOperation(value="Retorna todos usuarios cadastrados no banco")
	@GetMapping
	public ResponseEntity<List<Usuario>> findAll() {
		List<Usuario> listaUsuario = service.findAll();
		return ResponseEntity.ok().body(listaUsuario);
	}
	
	@ApiOperation(value="Retorna uma lista de digito único por usuário")
	@GetMapping(value="/lista-digito-unico/{id}")
	public ResponseEntity<List<DigitoUnico>> ListaDigitoPorUsuario (@PathVariable Long id) {
		List<DigitoUnico> listaDigitoUnico = service.listaDigitoPorUsuario(id);
		return ResponseEntity.ok().body(listaDigitoUnico);
	}

	@ApiOperation(value="Retorna um usuario pelo id")
	@GetMapping(value = "/{id}")
	public ResponseEntity<Usuario> findById(@PathVariable Long id) {
		Usuario usuario = service.findById(id);
		return ResponseEntity.ok().body(usuario);
	}

	@ApiOperation(value="Salva um usuario")
	@PostMapping
	public ResponseEntity<Usuario> save(@RequestBody Usuario usuario) {
		usuario = service.save(usuario);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(usuario.getId())
				.toUri();
		return ResponseEntity.created(uri).body(usuario);
	}

	@ApiOperation(value="Atualiza um usuario passando o id e o usuario atualizado")
	@PutMapping(value = "/{id}")
	public ResponseEntity<Usuario> update(@PathVariable Long id, @RequestBody Usuario usuario) {
		usuario = service.update(usuario, id);
		return ResponseEntity.ok().body(usuario);

	}

	@ApiOperation(value="Exclui um usuário pelo id")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Usuario> deleteById(@PathVariable Long id) {
		service.deleteById(id);
		return ResponseEntity.noContent().build();
	}
	
	@ApiOperation(value="Adiciona uma chave publica para determinado usuario")
	@PutMapping(value = "adiciona-chave-publica/{id}")
	public ResponseEntity<Usuario> criptografarInformacoesUsuario(@PathVariable Long id, @RequestBody String keyPublic) {
		Usuario usuario = service.criptografarInformacoesUsuario(id, keyPublic);
		return ResponseEntity.ok().body(usuario);

	}

}
