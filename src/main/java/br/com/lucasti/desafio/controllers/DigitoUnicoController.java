package br.com.lucasti.desafio.controllers;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.lucasti.desafio.entities.DigitoUnico;
import br.com.lucasti.desafio.services.DigitoUnicoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/digito-unico", produces = "application/json")
@Api(value="API REST Digito Unico")
public class DigitoUnicoController {
	
	@Autowired DigitoUnicoService service;
	
	
	
	@ApiOperation(value="Retorna o digito único, de um número passado: "
			+ "'N'(tamanho máximo de N = 10000000) concatenado por ele mesmo 'K' vezes(tamanho máximo de k = 100000")
	@PostMapping(value="/calcular")
	public ResponseEntity<DigitoUnico> save(@RequestBody DigitoUnico digitoUnico){
		digitoUnico = service.saveDigitoUnico(digitoUnico);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(digitoUnico.getId()).toUri();
		return ResponseEntity.created(uri).body(digitoUnico);
	}

}
