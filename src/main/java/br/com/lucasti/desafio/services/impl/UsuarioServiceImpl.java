package br.com.lucasti.desafio.services.impl;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lucasti.desafio.entities.DigitoUnico;
import br.com.lucasti.desafio.entities.Usuario;
import br.com.lucasti.desafio.repositories.UsuarioRepository;
import br.com.lucasti.desafio.security.Criptografia;
import br.com.lucasti.desafio.services.UsuarioService;
import br.com.lucasti.desafio.services.impl.exception.EntityNotFoundExceptionById;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	UsuarioRepository repository;

	@Override
	public List<Usuario> findAll() {
		return repository.findAll();
	}

	@Override
	public Usuario findById(Long id) {
		if (!repository.existsById(id)) {
			throw new EntityNotFoundExceptionById("Não existe usuario com esse id");
		}
		return repository.findById(id).get();
	}

	@Override
	public Usuario save(Usuario usuario) {
		return repository.save(usuario);
	}

	@Override
	public Usuario update(Usuario usuario, Long id) {
		Usuario usuarioBanco = findById(id);
		ModelMapper modelmapper = new ModelMapper();
		modelmapper.map(usuario, usuarioBanco);
		usuarioBanco.setId(id);
		return save(usuarioBanco);
	}

	@Override
	public void deleteById(Long id) {
		if (!repository.existsById(id))
			throw new EntityNotFoundExceptionById("Não existe usuario com esse id");
		repository.deleteById(id);
	}

	@Override
	public List<DigitoUnico> listaDigitoPorUsuario(Long id) {
		Optional<List<DigitoUnico>> listaDigitoUnico = repository.findById(id)
				.map(usuario -> usuario.getlistaDigitos());
		return listaDigitoUnico.get();
	}

	@Override
	public Usuario criptografarInformacoesUsuario(Long id, String keyPublic) {

		Usuario usuario = findById(id);
		usuario.setChavePublica(keyPublic);

		PublicKey publickey;

		publickey = Criptografia.genPublicKey(keyPublic);
		PrivateKey privatekey = Criptografia.genPrivateKey(Criptografia.CHAVE_PRIVADA);

		Criptografia criptografia = new Criptografia(publickey, privatekey);

		String nomeCriptografado = criptografia.criptografar(usuario.getNome());
		usuario.setNome(nomeCriptografado);

		String emailCriptografado = criptografia.criptografar(usuario.getEmail());
		usuario.setEmail(emailCriptografado);

		usuario.setNome(criptografia.descriptografar(nomeCriptografado));
		usuario.setEmail(criptografia.descriptografar(emailCriptografado));

		usuario = repository.save(usuario);

		return usuario;

	}

}
