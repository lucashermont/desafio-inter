package br.com.lucasti.desafio.services;

import java.util.List;

import br.com.lucasti.desafio.entities.DigitoUnico;
import br.com.lucasti.desafio.entities.Usuario;

public interface UsuarioService {
	
	public List<Usuario> findAll();
	
	public Usuario findById(Long id);
	
	public Usuario save(Usuario usuario);
	
	public Usuario update(Usuario usuario, Long id);
	
	public void deleteById(Long id);
	
	public List<DigitoUnico> listaDigitoPorUsuario(Long  id);

	public Usuario criptografarInformacoesUsuario(Long id, String keyPublic);

}
