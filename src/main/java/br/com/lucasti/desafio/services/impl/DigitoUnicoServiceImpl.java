package br.com.lucasti.desafio.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lucasti.desafio.entities.DigitoUnico;
import br.com.lucasti.desafio.repositories.DigitoUnicoRepository;
import br.com.lucasti.desafio.services.DigitoUnicoService;

@Service
public class DigitoUnicoServiceImpl implements DigitoUnicoService {

	@Autowired
	DigitoUnicoRepository repository;

	@Override
	public DigitoUnico saveDigitoUnico(DigitoUnico entity) {
		DigitoUnico digitoUnico = new DigitoUnico(entity.getN(), entity.getK(), entity.getUsuario());
		return repository.save(digitoUnico);
	}

}
